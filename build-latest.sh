#!/bin/bash

if [[ "$1" == "-t" || "$1" == "--tag" ]]; then    
    TAG="$2"
    shift 2  
else
    TAG="latest"
fi

docker build --pull --rm -f "dockerfile" -t vyushmanov/demosite:"$TAG" .
docker push vyushmanov/demosite:"$TAG"