#!/bin/bash

get-metadata() {
    helm get metadata demosite
}

release_name="demosite"
namespace="default"

resources=(\
"cm/demosite-version-page" \
"svc/demosite" \
"deploy/demosite" \
"ing/demosite" \
)

while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -n|--namespace)
            namespace="$2"
            shift
            ;;
        *)
            release_name="$1"
            ;;
    esac
    shift
done

echo "Release Name: $release_name"
echo "Namespace: $namespace"

echo "Checking if the app is Helm-managed..."
echo ""

if ! get-metadata 2> /dev/null
then 
    echo "Application is not managed by Helm. Applying labels and annotations..."
    for resource in "${resources[@]}"; do
        kubectl label "$resource" app.kubernetes.io/managed-by=Helm  
        kubectl annotate "$resource" meta.helm.sh/release-name="$release_name"
        kubectl annotate "$resource" meta.helm.sh/release-namespace="$namespace"
    done
fi