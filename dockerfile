FROM nginx:1.20

RUN ["rm", "-rf", "/usr/share/nginx/html"]
COPY src/html /usr/share/nginx/html

COPY src/default.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 80

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]

